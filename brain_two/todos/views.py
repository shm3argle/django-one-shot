from django.shortcuts import render
from .models import TodoList

def todo_list_list(request):
    todo_lists =TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/todos_list_list.html", context)


def todo_list_detail(request, id):
  todolist = get_object_or_404(TodoList, id=id)
  context = {
    "todolist": todolist
  }
  return render(request, "todos/todo_list_detail.html", context)
