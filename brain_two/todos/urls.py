from django.urls import path
from . import views
from .views import todo_list_detail

urlpatterns = [
    path("", views.todo_list_list, name="todo_list_list"),
    path("<int:id>/",todo_list_detail, name="todo_list_detail" ),
]
