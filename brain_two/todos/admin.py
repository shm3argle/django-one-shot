from django.contrib import admin
from .models import TodoList, TodoItem


class TodoListAdmin(admin.ModelAdmin):
    list_display = ("id", "name")


class TodoItemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date", "is_completed")


admin.site.register(TodoList, TodoListAdmin)
admin.site.register(TodoItem, TodoItemAdmin)
